var express = require('express');
var router = express.Router();
var passport = require('passport');
var userService = require('../services/user-service');

/* GET users listing. */
router.get('/', function(req, res) {
  res.send('respond with a resource users');
});

/* GET register page. */
router.get('/register', function(req, res, next) {
  var vm = {
      title: 'Create an account',
      page: 'register',
      error: ''
  };
  res.render('users/create', vm);
});

/*handle post*/
router.post('/register', function(req, res, next) {
    userService.addUser(req.body, function(err){
        console.log(err);
        if(err){  
        // console.log(err);
            var vm = {
              title: 'Create an account',
              page: 'register',
              input: req.body,
              error: err
            };
            delete vm.input.password;
            return res.render('users/create', vm);
        }
        req.login(req.body, function(err) {
          res.redirect('/orders');
        });
    });
});

router.post('/login', passport.authenticate('local'), function(req, res, next){
	res.redirect('/orders');
});

router.get('/logout', function(req, res, next) {
  req.logout();
  res.redirect('/');
});

module.exports = router;