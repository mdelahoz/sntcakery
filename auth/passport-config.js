module.exports = function() {
  var passport = require('passport');
  var passportLocal = require('passport-local');
  var userService = require('../services/user-service');
  
  passport.use(new passportLocal.Strategy({usernameField: 'email'}, function(email, password, next) {
    userService.findUser(email, function(err, user) {
      console.log(email);
      console.log(password);
      console.log(user);
      if (err) {
        console.log('error');
        console.log(err);
        return next(err);
      }
      if (!user || user.password !== password) {
        console.log('no error');
        return next(null, null);
      }
      next(null, user);
    });
  }));
  
  passport.serializeUser(function(user, next) {
    next(null, user.email);
  });
  
  passport.deserializeUser(function(email, next) {
    userService.findUser(email, function(err, user) {
      next(err, user);
    });
  });
};